/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author anapaulanevesferreira/ anarenauxmorais
 */
public class Pilot {
    
    private String name;
    private int rate;
    
    public Pilot(String name, int rate){
        this.name = name;
        this.rate = rate;
    }
    
    public String getName(){    
        return name;
    }
    
    public void setName(String name){
        this.name = name;     
    }
    
    public int getRate(){    
        return rate;
    }
    
    public void setRate(int rate){
        this.rate = rate;     
    }
    
    public String toString(){
        return "\nName: " + name +" Rate: " + rate; 
        
    }
    
 /*   public void ratingPilot(){
        
        switch(rate){
            case 1:
                System.out.println("Private");
                break;        
                
            case 2:
                System.out.println("Commercial");
                break;
                
            case 3:
                System.out.println("Instructor");
                break;
                
            case 4:
                System.out.println("Airline Transport");
                break;
                
            default:
                System.out.println("Not qualified");        
            
        }
    }*/
         
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author anapaulanevesferreira/ anarenauxmorais
 */
public class AirPlane {
    private String make;
    private int model;
    private int capacity;
    private Pilot pilot;
    private int rateAirplane;
    
    public AirPlane (String make, int model, int capacity, Pilot pilot){    
        this.make = make;
        this.model = model;
        this.capacity = capacity;
        this.rateAirplane = capacity / 100;
    }
    
    public String getMake(){    
        return make;
    }
    
    public void setMake(String make){
        this.make = make;     
    }
    
    public int getModel(){        
        return model;
    }
    
    public void setModel(int model){
        this.model = model;     
    }
    
     public int getCapacity(){        
        return capacity;
    }
     
     public void setCapacity(int capacity){        
        this.capacity = capacity;
    }
     
    public Pilot getPilot(){        
        return pilot;
    }
    
    public void assignPilot (Pilot pilot){
       this.pilot = pilot;
    }
    
    public String toString(){        
        return "\nAirplane Information:\nAircraft: \nMake:" + make + " Model: " + model + 
            "\n Capacity: " + capacity + " seats\nPilot: " + pilot;                       
    }      
}

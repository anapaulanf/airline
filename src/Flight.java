
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author anapaulanevesferreira/ anarenauxmorais
 */
public class Flight {
    String origin;
    String destination;
    String departureTime;
    String arrivalTime;
    String dateFlight;
    AirPlane assignedAircraft;
    
    public Flight(String origin, String destination, String dateFlight, AirPlane assignedAircraft){
       this.origin = origin;
       this.destination = destination;
       this.dateFlight = dateFlight;
       this.assignedAircraft = assignedAircraft;
    }
    
    public String toString(){
        return //"Flight Information:\nDate: " +dateFlight+ "From:" + origin + " to " +destination+ 
               // "\nFlight time: " +departureTime+ " to " + arrivalTime + assignedAircraft.toString();
        
        "Flight Information:\nDate: " +dateFlight+ "From:" + origin + " to " +destination+ 
        "\nFlight time: "+ arrivalTime + assignedAircraft.toString();
    }    
    
    public void schedule(String arrivalTime){
        this.arrivalTime = arrivalTime;    
    }
    
    public void schedule(String arrivalTime, String departureTime){
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
    }
}
